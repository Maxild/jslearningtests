module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine'], // jasmine 1.3.1 and karma-jasmine is bundled with karma by default
    autoWatch: true,
    files: [
        //"lib/jasmine-2.0.0-rc2/jasmine.js",
        //"lib/jasmine-2.0.0-rc2/jasmine-html.js",
        //"lib/jasmine-2.0.0-rc2/boot.js", // bootstrapping is done by jasmine adapter and karma, but we comment out frameworks
        "src/**/*.js",
        "test/SpecHelper.js",
        "test/**/*Spec.js"
    ],
    exclude: [],
    // possible values: 'dots', 'progress', 'junit', 'growl', 'coverage'
    reporters: ['progress'],
    port: 9876,
    colors: true,
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,
    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    browsers: ['Chrome'],
    // If browser does not capture in given timeout [ms], kill it
    captureTimeout: 60000,
    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: false
  });
};
