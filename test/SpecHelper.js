if (typeof getJasmineRequireObj === "function") {
    // 2.0.0-rc2 breaking changes
    beforeEach(function () {
        jasmine.Expectation.addMatchers({
            toBePlaying: function () {
                return {
                    compare: function (actual, expected) {
                        var player = actual;
                        return {
                            pass: player.currentlyPlayingSong === expected && player.isPlaying
                        }
                    }
                };
            }
        });
    });
}
else {
    // Karma uses jasmine 1.3.1
    beforeEach(function() {
        this.addMatchers({
            toBePlaying: function(expectedSong) {
                var player = this.actual;
                return player.currentlyPlayingSong === expectedSong &&
                    player.isPlaying;
            }
        });
    });
}
