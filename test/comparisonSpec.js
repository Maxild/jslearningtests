describe('string', function () {
    it('compared to string makes == operator work correctly', function () {
        expect("Morten").toBe("Morten");
    });
    it('compared to string makes === operator work correctly', function () {
        expect("Morten").toEqual("Morten");
    });
});

describe('operator ===', function () {
    it('null === null', function () {
        expect(null).toBe(null);
    });
    it('undefined === undefined', function () {
        expect(undefined).toBe(undefined);
    });
    it('NaN !== NaN', function () {
        expect(NaN).not.toBe(NaN);
    });
});

describe('Falsy values are', function () {
    it('null', function () {
        expect(null).toBeFalsy();
    });
    it('empty string', function () {
        expect("").toBeFalsy();
    });
    it('undefined', function () {
        expect(undefined).toBeFalsy();
    });
    it('0', function () {
        expect(0).toBeFalsy();
    });
    it('false', function () {
        expect(false).toBeFalsy();
    });
    it('NaN', function () {
        expect(NaN).toBeFalsy();
    });
});

describe('type information', function () {
    it('typeof string is "string"', function () {
        expect(typeof "Morten").toBe("string");
    });
    it('typeof new String is "object"', function () {
        expect(typeof new String("Morten")).toBe("object");
    });
    it('typeof 12 is "number"', function () {
        expect(typeof 12).toBe("number");
    });
    it('typeof new Number is "object"', function () {
        expect(typeof new Number(12)).toBe("object");
    });
    it('typeof null is "object"', function () {
        expect(typeof null).toBe("object");
    });
    it('typeof undefined is "undefined"', function () {
        expect(typeof undefined).toBe("undefined");
    });
    it('typeof false is "boolean"', function () {
        expect(typeof false).toBe("boolean");
    });
    it('typeof true is "boolean"', function () {
        expect(typeof true).toBe("boolean");
    });
    it('typeof Date is "object"', function () {
        expect(typeof new Date()).toBe("object");
    });


    it('isNaN: x !== x', function () {
        expect(NaN === NaN).toBeFalsy();
        expect(NaN !== NaN).toBeTruthy();
    });
    it('isArray native', function () {
        expect(Array.isArray([])).toBe(true);
    });
    it('isArray polyfill', function () {
        var isArray = function(arr) {
            return Object.prototype.toString.apply(arr) === "[object Array]";
        };
        expect(isArray([])).toBe(true);
    });
});

describe('Object wrappers', function () {
    it('for primitive types does boxing and therefore expando is not possible', function () {
        "hello".someProperty = 16;
        expect("hello".someProperty).toBeUndefined();
    });
    it('=== does not work normally', function () {
        expect(new String("Morten") === new String("Morten")).toBeFalsy();
    });
    it('== does not work normally', function () {
        expect(new String("Morten") == new String("Morten")).toBeFalsy();
    });
});

describe('prototype and constructor properties', function () {
    it('new Object will create new object that inherits from Object.prototype and has constructor equal to Object', function () {
        var obj = new Object();
        expect(Object.getPrototypeOf(obj)).toBe(Object.prototype);
        expect(obj.constructor).toBe(Object);
    });
    it('Object.create(Object.prototype) will create new object that inherits from Object.prototype and has constructor equal to Object', function () {
        var obj = Object.create(Object.prototype);
        expect(Object.getPrototypeOf(obj)).toBe(Object.prototype);
        expect(obj.constructor).toBe(Object);
    });
    it('literal object {} will create new object that inherits from Object.prototype and has constructor equal to Object', function () {
        var obj = {};
        expect(Object.getPrototypeOf(obj)).toBe(Object.prototype);
        expect(obj.constructor).toBe(Object);
    });
    it('new MyCtor will create new object that inherits from MyCtor.prototype and has constructor equal to MyCtor', function () {
        function MyCtor(){}
        var myObject = new MyCtor();
        // prototype is not enumerable on created object
        //expect(myObject.prototype).toBe(MyCtor.prototype);
        expect(Object.getPrototypeOf(myObject)).toBe(MyCtor.prototype);
        expect(myObject.constructor).toBe(MyCtor);
    });
    it('Object.create(proto) will create new object that inherits from proto argument and has constructor equal to Object', function () {
        var proto = {};
        var obj = Object.create(proto);
        expect(Object.getPrototypeOf(obj)).toBe(proto);
        // because proto does not have a constructor property the prototype chain is searched,
        // and Object.prototype has a constructor property
        expect(obj.constructor).toBe(Object.prototype.constructor);
        expect(Object.prototype.constructor).toBe(Object);
    });
    it('primitive string will have constructor == String', function () {
        expect("a string".constructor).toBe(String);
    });
});

function getGlobalObj() {
    return this;
}

if (typeof Object.create !== 'function') {
    Object.create = function (o) {
        // F.
        function F() {}
        //
        F.prototype = o;
        // new operator will create a new object that inherits from F.prototype
        return new F();
    };
}

if (typeof Object.keys !== "function") {
    Object.keys = function( obj ) {
        var array = [];
        for ( var prop in obj ) {
            if ( obj.hasOwnProperty( prop ) ) {
                array.push( prop );
            }
        }
        return array;
    };
}
var util = (function(){
    function isFunction(obj) {
        return typeof obj === 'function';
    }
    function getPrototypeOf(obj) {
        if (Object.getPrototypeOf) {
            try {
                // Throws a TypeError, if the obj isn't an Object (e.g. primitive number/string)
                return Object.getPrototypeOf(obj);
            }
            catch (err) {
                // We use the constructor property of the boxed object
                return obj.constructor.prototype;
            }
        }
        return obj.__proto__ || obj.constructor.prototype;
    }
    function getOwnPropertyNames(obj) {
        if (Object.getOwnPropertyNames) {
            try {
                // Throws a TypeError, if the obj isn't an Object (e.g. primitive number/string)
                return Object.getOwnPropertyNames(obj);
            }
            catch (err) {
                // We use the constructor property of the boxed object
                return Object.getOwnPropertyNames(new obj.constructor);
            }
        }
        // we cannot polyfill getOwnPropertyNames using javascript/ECMAScript
        return Object.keys(obj);
    }

    // TODO: use jasmine.log (add support in jasmine adapter)
    // Note: jasmine.log does not exist!!!! use console.log
    function inspect(obj, own) {
        var testObj = obj || getGlobalObj();
        var results = [];
        // for...in only sees enumerable properties
        for (var name in testObj) {
            if (own && !testObj.hasOwnProperty(name)) {
                continue;
            }
            var val = testObj[name];
            if (isFunction(val)) {
                results.push(name + " [Function]");
            }
            else {
                results.push(name + " = " + val)
            }
        }
        return results;
    }

    function getProtoChainOf(obj) {

        var knownPrototypes = [
            Object.prototype,
            Array.prototype,
            String.prototype,
            Boolean.prototype,
            RegExp.prototype,
            Number.prototype
        ];
        var protoNames = [
            "Object.prototype",
            "Array.prototype",
            "String.prototype",
            "Boolean.prototype",
            "RegExp.prototype",
            "Number.prototype"
        ];
        var testObj = obj || getGlobalObj();

        results = [];
        while (testObj) {
            var j = knownPrototypes.indexOf(testObj);
            results.push(j >= 0 ? protoNames[j] : Object.prototype.toString.apply(testObj));
            testObj = getPrototypeOf(testObj);
        }

        return results;
    }

    // TODO: enumerable
    function inspectDeep(obj, protoChain) {
        var testObj = obj || getGlobalObj();
        var queryObj = testObj;
        var name,
            val,
            level = 1,
            levelString,
            results = [];

        // TODO: exclude Object.getOwnPropertyNames(Object.prototype) that starts with __
        while (queryObj) {
            var names = getOwnPropertyNames(queryObj);
            for (var i = 0; i < names.length; i++) {
                name = names[i];
                val = testObj[name];
                // creates string of level-1 repeated '*' characters
                levelString = new Array(level).join("*");
                if (isFunction(val)) {
                    results.push(name + levelString);
                }
                else {
                    results.push(name + levelString + " = " + val)
                }
            }
            if (!protoChain) {
                break
            }
            queryObj = getPrototypeOf(queryObj);
            level += 1;
        }

        return results;
    }

    return {
        inspect: inspectDeep,
        getProtoChainOf: getProtoChainOf
    }
})();