describe('Array.prototype learning by example:', function() {

  it("splice can remove elements (in a destructive way)", function() {
    var arr = [1, 2, 3, 4, 5];
    arr.splice(2, 2);

    expect(arr).toEqual([1, 2, 5]);
  });

  it("splice should return removed items", function() {
    var arr = [1, 2, 3, 4, 5];
    var result = arr.splice(2, 3);

    expect(result).toEqual([3, 4, 5]);
  });

  it("splice can add elements (in a destructive way)", function() {
    var arr = [1, 2, 3, 4, 5];
    arr.splice(2, 0, 6, 7);

    expect(arr).toEqual([1, 2, 6, 7, 3, 4, 5]);
  });
});